import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class AvitoLocators {
    private static WebDriver driver;

    private static String START_AVITO = "https://www.avito.ru/moskva/transport";

    public static List getAllMotoLocators(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(START_AVITO);
        driver.findElement(By.xpath("//a[@title='Мотоциклы и мототехника в Москве']")).click();
        driver.findElement(By.xpath("//a[@title='Мотоциклы']")).click();
        List<WebElement> moto = driver.findElements(By.xpath("//span[@itemprop='name']]"));
        //List all = null;
        //for(WebElement e: moto){
        //    all.add(e.getText());
        //}
        driver.quit();
        return moto;
    }

    public static List getAllAutoTodayLocators(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(START_AVITO);
        driver.findElement(By.xpath("//a[@title='Автомобили в Москве']")).click();
        List auto = driver.findElements(By.xpath("//div[@class='created-date'] and contains(text(),'Сегодня')"));
        driver.quit();
        return auto;
    }

    public static String getAudi(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(START_AVITO);
        driver.findElement(By.xpath("//a[@title='Автомобили в Москве']")).click();
        driver.findElement(By.xpath("//a[@title='Audi']")).click();
        String audi = driver.findElement(By.xpath("//span[@itemprop='name']")).getText();
        driver.quit();
        return audi;
    }

    public static void main(String[] args) {
        System.out.println(AvitoLocators.getAudi());
        System.out.println(AvitoLocators.getAllAutoTodayLocators());
        System.out.println(AvitoLocators.getAllMotoLocators());
    }
}
