import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class TestRegistration {

    private static WebDriver driver;

    public static String URL = "https://www.survio.com/";
    public static String URL_STEP = "https://my.survio.com/survey-wizard/start?new=1";

    enum XPATHLOCATORS {
        name("/html/body/section[1]/div/form/div[1]/input"),
        email("//*[@id=\"reg_email\"]"),
        passwd("/html/body/section[1]/div/form/div[3]/input"),
        createbutton("/html/body/section[1]/div/form/div[4]/a");
        private String value;
        XPATHLOCATORS(String value){
            this.value = value;
        }
        public String getXPath(){
            return value;
        }
    }

    @BeforeClass
    public static void setupChrome(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(URL);
    }

    //Positive test
    @Test
    public void testRegistrationPositive(){
        try {
            WebElement name = driver.findElement(By.xpath(XPATHLOCATORS.name.getXPath()));
            name.clear();
            name.sendKeys("John");

            WebElement email = driver.findElement(By.xpath(XPATHLOCATORS.email.getXPath()));
            email.clear();
            email.sendKeys("my_mail_4321@ya.ru");

            WebElement passwd = driver.findElement(By.xpath(XPATHLOCATORS.passwd.getXPath()));
            passwd.clear();
            passwd.sendKeys("12345678");

            driver.findElement(By.xpath(XPATHLOCATORS.createbutton.getXPath())).submit();
        }
        catch (NoSuchElementException e){
            Assert.fail("Element not found");
        }

        Assert.assertEquals("Page is not load", URL_STEP, driver.getCurrentUrl());
    }

    //Negative test
    @Test
    public void testRegistrationNegative(){
        try{
            driver.findElement(By.xpath(XPATHLOCATORS.createbutton.getXPath())).submit();
        }
        catch (NoSuchElementException e){
            Assert.fail("Create button is not found");
        }

        Assert.assertTrue("Error create discussion", driver.findElement(By.xpath(XPATHLOCATORS.createbutton.getXPath())).isDisplayed());
    }

    @AfterClass
    public static void killDriverChrome(){
        driver.quit();
    }
}
